/**
 * Created by Yuri on 14/07/2017.
 */

var main = function () {

    var buildTemplate = function () {
        var template = '<div class="svg-box">' +
            '<img src="logo.svg">' +
            '</div>' +
            '<div class="rating">' +
            '<img>' +
            '<span>Rated <span id="rating">..</span> out of 5</span>' +
            '</div>' +
            '<div class="panel-group" id="panel-group-id"></div>';

        document.getElementById('tp-widget').innerHTML = template;
    };
    buildTemplate();

    //Accordion
    var applyAccordion = function () {

        var accordion = document.getElementsByClassName('panel-heading');

        for (var i = 0; i < accordion.length; i++) {
            accordion[i].onclick = function () {

                /* Animation */
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + 'px';
                }
            }
        }
    };
    var sumRating = 0;
    var reviewsCounter = 0;

    //Display the reviews
    reviews.forEach(function (review) {
        reviewsCounter++;
        sumRating = sumRating + parseInt(review.starRating);

        var panelClass = 'panel';

        var addReview =
            '<div class=' + panelClass + '>' +
            '<div class="panel-heading">' +
            '<h3>' + review.firstName + ' ' + review.lastName +
            '</h3>' + '<h4>' + review.reviewTitle + '</h4>' +
            '</div>' +

            '<div class="panel-collapse collapse">' +
            '<p>' + review.reviewBody +
            '</p>' +
            '</div>' +
            '</div>';

        document.getElementById('panel-group-id').innerHTML += addReview;
    });

    //Display the average rating number
    var averageRating = sumRating / reviewsCounter;
    document.getElementById('rating').innerHTML = averageRating.toString();

    //Display STARS rating image
    document.querySelector('.rating img').src = 'static-images/' + Math.round(averageRating) + '-stars-260x48.png';

    applyAccordion();
};

main();
